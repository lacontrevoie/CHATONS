Le process de modération suivant a été [voté le 1er mars 2021](https://forum.chatons.org/t/process-de-moderation-du-forum/2063/25).

## Objet

L'équipe de modération a la responsabilité de mettre un terme aux comportements contraires à la charte du collectif ou à la loi.

## Membres de l'équipe de modération

Les personnes qui souhaitent être membres de l'équipe de modération se proposent en annonçant leur candidature dans la [catégorie collectif du forum](https://forum.chatons.org/c/collectif/20). S'il y a consensus, elles intègrent l'équipe deux semaines après, pour une période d'un an. À l'issue de cette période elles doivent de nouveau présenter leur candidature.

La personne membre de l'équipe de médiation qui a agi est à priori responsable de l'action de modération, si elle devient nécessaire.

## Contacter la modération

L'équipe de modération peut être contactée à tout moment en envoyant un message privé à l'une des personnes qui la composent ou à l'utilisateur [moderateur](https://forum.chatons.org/u/moderateur). Les discussions qui en découlent sont privées et seront divulguées uniquement selon les modalités du process de modération.

## Process de modération (obligatoire)

L'équipe de modération agit lorsqu'elle le pense nécessaire et doit impérativement:

* S'assurer auprès de [l'équipe de médiation](https://forum.chatons.org/g/equipe_mediation) qu'une action de modération est souhaitable.
* Faire l'action de modération avec le compte [moderateur](https://forum.chatons.org/u/moderateur).
* Documenter l'action dans la [catégorie collectif](https://forum.chatons.org/c/collectif/20) sans y inclure des éléments qui permettent d'identifier directement l'objet de la modération et les personnes impliquées.
* Documenter l'action dans la [catégorie modération](https://forum.chatons.org/c/collectif/moderation/84) avec tous les détails nécessaires pour que les membres du collectif puissent juger de sa pertinence, c'est-à-dire tous les éléments factuels et le raisonnement qui conclut que les comportements sont contraires à la charte ou à la loi.
* Répondre aux questions des membres du collectif visant à éclairer la décision.
* Supprimer l'explication de l'action de modération un an après sa publication afin de respecter le droit à l'oubli des personnes visées.
* Si la demande en est faite par une personne du collectif, mettre au vote sans délai une décision visant à inverser l'action de modération.

Il n'est pas nécessaire de respecter ce process lorsque les comptes visés correspondent sans doute possible à des robots.

## Guide de modération (facultatif)

Il est suggéré que les actions de modération suivent les principes suivants. Leur application est à la discrétion de l'équipe de modération, en fonction des circonstances. L'explication détaillée de l'action de modération et les discussions qui suivent sont l'occasion pour chaque membre du collectif de demander à l'équipe de médiation de justifier pour quelle raison elle a dévié de ce guide.

* Une personne est exclue du forum après avoir été avertie plusieurs fois que ses comportements sont contraires à la charte ou à la loi.
* Une personne est exclue du forum après s'être comportée de façon contraire à la charte ou à la loi et avoir déclaré sans doute possible son intention de répéter ces comportements.
* Lorsqu'une personne est exclue du forum, son compte est anonymisé et l'explication détaillée fait référence à son compte anonymisé et non à son compte avant anonymisation.
* Une personne membre de l'équipe de modération agit en concertation avec les autres membres de l'équipe et non unilatéralement.
* La durée d'exclusion d'une personne est au minimum de trois mois et doit être la plus courte possible. Elle s'apprécie en évaluant la quantité de travail que requiert une exclusion et la probabilité qu'elle se répète lorsque la personne est réintégrée au forum.
* Si la personne exclue fait la demande à l'équipe de modération (en écrivant à
moderation@chatons.org) d'être réintégrée de façon anticipée, cette demande est ajoutée au fil de discussion concernant la décision d'exclusion dans la [catégorie modération](https://forum.chatons.org/c/collectif/moderation/84). L'équipe de modération répond à la demande et justifie sa réponse.
